import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int _count = 0;

  void _incr() {
    setState(() {
      _count++;
    });
  }

  void _reset() {
    setState(() {
      _count = 0;
    });
  }

  void _decr() {
    setState(() {
      _count--;
    });
  }

  Widget _actions() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _purpleButton('Increment', _incr),
          _purpleButton('Reset', _reset),
          _purpleButton('Decrement', _decr),
        ],
      );

  Widget _counter() => Text('$_count', style: TextStyle(fontSize: 50));

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _counter(),
        _actions(),
      ],
    );
  }
}

ElevatedButton _purpleButton(String text, Function() onClick) {
  return ElevatedButton(
    onPressed: onClick,
    child: Text(text),
    style: ElevatedButton.styleFrom(primary: Colors.purple),
  );
}
