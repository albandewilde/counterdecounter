import 'package:flutter/material.dart';

import 'counter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Counter Decounter',
      home: Scaffold(
        body: Center(
          child: Counter(),
        ),
      ),
    );
  }
}
